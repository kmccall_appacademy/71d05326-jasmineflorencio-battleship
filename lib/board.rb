class Board
  attr_accessor :grid
  def initialize(grid= nil)
    if grid == nil
      @grid = Board.default_grid
    else
      @grid = grid
    end
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def counter
    counter = Hash.new(0)
    self.grid.each do |row|
      row.each do |spot|
        counter[spot] += 1
      end
    end
    counter
  end

  def count
    counter[:s]
  end

  def empty?(position= nil)
    if position == nil
      return true if counter.keys.count == 1 && self.grid[0][0] == nil
    else
      return true if self.grid[position[0]][position[1]] == nil
    end
    false
  end

  def full?
    return true unless counter.keys.include?(nil)
  end

  def open_positions
    [].tap do |open_array|
      self.grid.each_with_index do |row, idx|
        row.each_with_index do |spot, idx2|
          open_array << [idx, idx2] if spot == nil
        end
      end
    end
  end

  def place_random_ship
    raise "The grid is full." if self.full?
    ship_place = open_positions.sample
    self.grid[ship_place[0]][ship_place[1]] = :s
  end

  def won?
    true unless counter.keys.include?(:s)
  end

  def [](index)
    grid[index[0]][index[0]]
  end



end
