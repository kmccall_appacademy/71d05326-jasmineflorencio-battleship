class BattleshipGame
  attr_reader :board, :player
  def initialize(player, board)
    @player = player
    @board = board
  end


  def attack(position)
    self.board.grid[position[0]][position[1]] = :x
  end

  def count
    self.board.count
  end

  def game_over?
    self.board.won?
  end

  def play_turn
    move = player.get_play
    self.attack(move)
  end

end
